#!/usr/bin/python -u

import re, fileinput, sys

wordgiven = sys.argv[1]
found = []
for line in sys.stdin:
        line = re.sub('[^a-zA-Z ]', ' ', line)
        line = re.sub(' +', ' ', line)
        for word in line.split():
                found.append(word)
#print "%s" % found
#print "%d" %len(found)
count = 0
for element in found:
	eupper = element.upper()
	elower = element.lower()
	wordupper = wordgiven.upper()
	wordlower = wordgiven.lower()
	#print "comparing %s with %s and %s with %s" %(eupper, wordupper, elower, wordlower)
	if (eupper == wordupper or elower == wordlower):
		#print "match found"
		count += 1

print "%s occurred %d times" %(wordlower, count)

