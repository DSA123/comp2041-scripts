#!/usr/bin/perl -w

$swap = 0;
$max = 0;

while () {
	if ($swap == 0) {
		print "Enter student name: ";
		$name = <STDIN>;
		chomp($name) if defined $name;
		$swap++;
	} else {
		if (defined $name) {
			print "Enter library fine: ";
			$fine = <STDIN>;
			chomp($fine) if defined $fine;
			$swap--;
			$student{$name} += $fine;	
			if ($student{$name} >= $max) {
				$max = $student{$name};
				$maxstudent = $name;
			}
		} else {
			last;
		}
	}
}

print "\n";
print "Expel ".$maxstudent.' whose library fines total $'.$student{$maxstudent}."\n";	
