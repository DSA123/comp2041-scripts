#!/usr/bin/perl -w

foreach $arg (@ARGV) {
	$seen = 0;
	foreach $var (@vars) {
		if ($var eq $arg) {
			$seen = 1;
			last; 
		}		
	}
	if ($seen == 0) {
		push @vars, $arg;
	}
}

foreach $var (@vars) {
	print $var." ";
}
print "\n";
