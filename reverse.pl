#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@fields = split" ", $line;
	push @allfields, "\n";
	push @allfields, @fields;
}

@reverse = reverse(@allfields);

$seen = 0;
foreach $var (@reverse) {
	if ($var =~ /\n/) {
		$seen = 1;
	}
	if ($seen == 0) {
		print $var." ";
	} else {
		print $var;
	}
	$seen = 0;
}
#print join(" ", @reverse)."\n";
