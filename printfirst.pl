#!/usr/bin/perl -w

foreach $arg (@ARGV) {
	print $arg." " if (! defined $seen{$arg});
	$seen{$arg}++;
}
print "\n";
