#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	$line =~ s/ +/ /g;
	@numbers = split " ", $line;
}
$max = 0;
$seen = 0;
$ascent = 0;
$descent = 0;
$apex = 0;
$last = 0;

foreach $val (@numbers) {
	if (defined $prevval) {
		if ($prevval < $val) {
			push @ascending, $val;
			$ascent = 1;
		} elsif ($prevval > $val) {
			push @descending, $prevval;
			$descent = 1;
		}
	} else {
		push @ascending, $val;
	}
	if ($val > $max) {
		$max = $val;
	} elsif ($val == $max) {
		$seen++;
	}
	$prevval = $val;
	$last = $val;
}

push @descending, $last;

if (($max > 0) &&  ($seen == 0)) {
	$apex = 1;
}

if ($ascending[$#ascending] == $descending[0]) {
	pop @ascending;
}

push @allnum, @ascending;
push @allnum, @descending;

if ((@allnum == @numbers) && ($apex == 1) && ($ascent == 1) && ($descent == 1)) {
	print "hill\n";
} else {
	print "not hill\n";
}	
#print "Apex is ".$apex."\n";
#print join(" ", @allnum)."\n";
#print join(" ", @numbers)."\n";
