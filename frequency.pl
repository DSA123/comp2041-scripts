#!/usr/bin/perl -w
#trash comment to allow git commit to test autotests
$wordgiven = $ARGV[0];

foreach $file (glob "poets/*.txt") {
        #print "$file\n";
        open($fh, $file) or die;
        while (<$fh>) {
                $_ =~ s/[^a-zA-Z ]/ /g;
                $_ =~ s/ +/ /g;
                @found = grep /([a-zA-Z]+)/, split ' ', $_;
                #$count += @found;
                #print "Found $count1\n";
                push @words, @found;
                #print "words found are: @found\n";
        }
        $found = 0;
        foreach $f (@words) {
                $fupper = uc $f;
                $flower = lc $f;
                $wordupper = uc $wordgiven;
                $wordlower = lc $wordgiven;
                if (("$fupper" eq "$wordupper") || ("$flower" eq "$wordlower")) {
                        $found++;
                }
        }
        $total_words = scalar @words;
        @file_name = grep /([a-zA-Z]+)/, split '[^a-zA-Z]+', $file;
        shift @file_name;
        pop @file_name;
        $freq_number = $found / $total_words;
        $file_name_string = join(" ", @file_name);
        printf "%4d/%6d = %.9f %s\n", $found, $total_words, $freq_number, $file_name_string;
        #print "$found / $total_words = test number ... @file_name\n";
        #print "$wordlower occurred $found times\n";
        $found = 0;
        @found = ();
        @words = ();
        @file_name = ();
}     
