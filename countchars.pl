#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@chars = split "", $line;
	push @allchars, @chars;
}

foreach $char (@allchars) {
	if ($char =~ /[a-zA-Z0-9]/) {
		$count{$char}++;
	}
}

foreach $key (sort keys %count) {
	print "'".$key."' occured ".$count{$key}." times\n";
}
