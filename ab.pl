#!/usr/bin/perl -w

if ($#ARGV >= 0) {
	$arg = $ARGV[0];
	@chars = split '', $arg;
	foreach $char (@chars) {
		if ($char =~ /a|b/) {
			$count{$char}++;
		}
	}
	if (defined $count{"a"} && defined $count{"b"}) {
		if ($count{"a"} == $count{"b"}) {
			print "yes\n";
		} else {
			print "no\n";
		}
	} else {
		print "no\n";
	}
} else {
	print "no\n";
}
