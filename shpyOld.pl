#!/usr/bin/perl -w

# written by andrewt@cse.unsw.edu.au August 2015
# as a starting point for COMP2041/9041 assignment 
# http://cgi.cse.unsw.edu.au/~cs2041/assignment/shpy
%imports = ();
$indent = 0; #false

while ($line = <>) {
	if ($line =~ /^cd *|\s+test\s+/) {
                if (!exists($imports{"os"})) {
                        $imports{"os"} = "os";
                }
        } elsif ($line =~ /^ls *|^pwd *|^id *|^date */) {
		if (!exists($imports{"subprocess"})) { 
			$imports{"subprocess"} =  "subprocess";
		}
	} elsif ($line =~ /\s*exit\s*|\s*read\s*|\$[0-9]+\s*/) {
		if (!exists($imports{"sys"})) {
			$imports{"sys"} = "sys";
		}
	} elsif ($line =~ /\*\..+/) {
		if (!exists($imports{"glob"})) {
			$imports{"glob"} = "glob";
		}
	}
	chomp $line;
	push @stored_lines, $line;
}
$line_count = 0;
foreach $line (@stored_lines) {
    if ($line =~ /^#!/ && $line_count == 0) {
	print "#!/usr/bin/python2.7 -u\n";
	for (sort keys %imports) {
        	print "import $imports{$_}\n";
	}
    } elsif ($line =~ /cd/) {
	@cdcontents = split ' ', $line;
	shift @cdcontents;
	foreach $text (@cdcontents) {
		if ($indent == 1) {
			print "\t";
		}
		print "os.chdir(\'$text\')\n";
	}
    } elsif ($line =~ /echo/) {
	@words = grep /(.+)/, split ' ', $line;
        shift @words;
	if ($indent == 1) {
		print "\t";
	}
	print "print ";
	if ($words[0] =~ /'.+/) {
		print join(' ',@words);
	} elsif ($words[0] =~ /".+/) {
		print join(" ", @words);
	} else {
		$i = 0;
		foreach $text (@words) {
			if ($text =~ /\$[0-9]+\s*/) {
				$text =~ s/\$//;
				print "sys.argv[$text]";
			} elsif ($text =~ /\$.+/) {
				$text =~ s/\$//;
				print "$text";
			} else {
				print "\'$text\'";
			}
			if ($i < $#words) {
				print ", ";
			}
			$i++;
		}
	}
	print "\n";
	#print "print ";
	#print "'",join ( "', '", @words),"'\n";
    } elsif ($line =~ /for/) {
	$indent = 1;
	@forloop = split ' ', $line;
	$count  = 0;
	foreach $text (@forloop) {
		#first 3 words are "for blah in"	
		if ($count < 3) {
			print "$text ";
		} else {
			if ($text =~ /[0-9]+/) {
				print "$text";
			} elsif ($text =~ /\*\..+/) {
				print 'sorted(glob.glob("'."$text".'"))';
			} else {
				print "\'$text\'";
			}
			if ($count < $#forloop) {
				print ", ";
			} else {
				print ":\n";
			}
		}
	$count++;
	}	  
    } elsif ($line =~ /^if *|^elif *|^else */) {
	$indent = 1;
        @if_words = split ' ', $line;
	$if_count = 0;
        foreach $text (@if_words) { 
                if ($if_count < 1 || $text =~ /[^a-zA-Z0-9]+/) {
                        if ($text =~ /=/) {
				print "==";
			} else {
				print "$text";
			}
			if ($if_count < $#if_words) {
                        	print " ";
                	} else {
                        	print ":\n";
                	}
                } else {
                        if ($text !~ /test/) {
				print "\'$text\'";
				if ($if_count < $#if_words) {
                        		print " ";
                		} else {
                        		print ":\n";
				}
			}
                }
	$if_count++;
	}
    } elsif ($line =~ /ls|pwd|id|date/) {
	@commands = grep /(.+)/, split ' ', $line;
	if ($indent == 1) {
		print "\t";
	}
	print "subprocess.call([";
	$j = 0;
	foreach $text (@commands) {
		if ($j == 0) {
			print "\'$text\'";
		} else {
			print ", \'$text\'";
		}
		$j++;
	}
	print "])";
	print "\n";
    } elsif ($line =~ /[a-zA-Z_]+[a-zA-Z0-9_]*=.+/) {
        @variables = split /=/, $line;
	$varcontents = pop @variables;
	$varname = pop @variables;
        print "$varname = \'$varcontents\'\n";
    } elsif ($line =~ /read/) {
	if ($indent == 1) { 
		print "\t";
	}
	@read_line = split / /, $line;
	$read_name = pop @read_line;
	print "$read_name = sys.stdin.readline().rstrip()\n";
    } elsif ($line =~ /exit/) {
	if ($indent == 1) {
		print "\t";
	}
	@exitnumber = grep /(.+)/, split ' ', $line;
	$number = pop @exitnumber;
	print "sys.exit($number)\n";
    } elsif ($line =~ /do|then/) {
	print "";
    } elsif ($line =~ /done|fi/) {
	$indent = 0;
    } elsif ($line =~ /^$/) {
	print "\n";
    } else {
        # Lines we can't translate are turned into comments
        print "#$line\n";
    }
$line_count++;
}
