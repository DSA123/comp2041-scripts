#!/bin/bash
#set -x
if [[ $# -eq 0 ]]
then
        echo "Usage: <image> <image> ..."
        exit 1
else
        for var in "$@"
        do
		label=`ls -l "$var" | cut -d" " -f8-12 | egrep -o "[^0-9]*[0-9]{2} +[A-Z]{1}[a-z]{2} +[0-9]{2}:*[0-9]{2}" | sed -e 's/  / /g' -e 's/^ //g'`
		label2=`printf "'%s'" "$label"`
		#echo "$label"
		#echo "$label2"
		century=`echo "$year" | egrep -o "[0-9]{4}" | egrep -o "^[0-9]{2}"`
		year=`echo "$label" | egrep -o "[0-9]{4}" | egrep -o "[0-9]{2}$"`
		month=`echo "$label" | egrep -o "[A-Z]{1}[a-z]{2}"`
		if [[ "$month" =~ "Jan" ]]
		then
			month=01
		elif [[ "$month" =~ "Feb" ]]
                then
		        month=02
		elif [[ "$month" =~ "Mar" ]]
                then
		        month=03
		elif [[ "$month" =~ "Apr" ]]
                then
		        month=04
                elif [[ "$month" =~ "May" ]]
                then
		        month=05
		elif [[ "$month" =~ "Jun" ]]
                then
		        month=06
                elif [[ "$month" =~ "Jul" ]]
                then
		        month=07
                elif [[ "$month" =~ "Aug" ]]
                then
		        month=08
                elif [[ "$month" =~ "Sep" ]]
                then
			month=09
		elif [[ "$month" =~ "Oct" ]]
                then
			month=10
                elif [[ "$month" =~ "Nov" ]]
                then
			month=11
                elif [[ "$month" =~ "Dec" ]]
                then       
			 month=12
                fi
		day=`echo "$label" | egrep -o "^[0-9]{2}"`
		hour=`echo "$label" | egrep -o "[0-9]{2}:[0-9]{2}" | egrep -o "^[0-9]{2}"`
		mins=`echo "$label" | cut -d":" -f2`
		#echo "hour is $hour"
		#echo "mins is $mins"
		if ! [[ "$hour" =~ ^[1-9]{2}$ ]]
		then
			hour=00
		fi
		if ! [[ "$mins" =~ ^[1-9]{2}$ ]]
		then
			mins=00
		fi 
		convert -gravity south -pointsize 36 -draw "text 0,10 $label2" "$var" "$var" 
		#``[[CC]YY]MMDDhhmm[.SS]''
		touchstr=`printf "%s%s%s%s%s%s" "$century" "$year" "$month" "$day" "$hour" "$mins"`
		#echo "$touchstr"
		#echo "hour is $hour"
		#echo "mins is $mins"
		touch -t "$touchstr" "$var"
	done
fi
#convert -gravity south -pointsize 36 -draw "text 0,10 'Andrew rocks'" penguins.jpg temporary_file.jpg
