#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	$species = $line;
	$number = $line;
	$number =~ s/ .*//;
	$species =~ s/^[0-9]+ *//;
	$species = lc($species);
	$species =~ s/ *$//;
	$species =~ s/s$//;
	$species =~ s/ +/ /;
	$pods{$species}++;
	$individuals{$species} += $number;
}

foreach $key (sort keys %individuals) {
	print $key. " observations:  ".$pods{$key}." pods, ".$individuals{$key}." individuals\n";
}
