#!/bin/sh

count=0
number=$1
if test $# != 2
then
	echo "Usage: ./echon.sh <number of lines> <string>"
else
	if ! [[ "$number" =~ ^[0-9]+$ ]]
	then
		echo "./echon.sh: argument 1 must be a non-negative integer"
	else
		while test "$count" -lt "$number"; do
			echo "$2"
			count=`expr $count + 1`
		done		
	fi
fi
