#!/bin/bash

i=2
j=0
while [ $i -le `expr $1 / 2` ]; do
	if [ `expr $1 % $i` -eq 0 ]; then
		j=1
	fi
	$((i + 1))
done
if [ $j -eq 1 ]; then
	echo "$1 is not prime"
else
	echo "$1 is prime"
fi
