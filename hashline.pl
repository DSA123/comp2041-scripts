#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	push @lines, $line;
}

foreach $text (@lines) {
	if ($text =~ /^#[0-9]+$/) {
                $number = $text;
                $number =~ s/#//;
                $text = $lines[$number - 1];
        }
	push @final, $text;
}

print "\n";
foreach $var (@final) {
	print $var."\n";
}
