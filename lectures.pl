#!/usr/bin/perl -w

#$course = $ARGV[0];

foreach $course (@ARGV) {
	@page_content = `wget -q -O- http://www.timetable.unsw.edu.au/current/$course.html`;
	$count = 0;
	$seen = 0;
	foreach $line (@page_content) {
		chomp($line);
		if ($line =~ /<a href.*Lecture</) {
			#print "$line\n";
			$seen = 1;
			#print "$line\n";
			if ($line =~ /.+\#(S1|S2)\-.+/) {
				$semester = $1;
				$lecture_codes{$course} = $semester;
				#print "semester is: $semester\n";
			}
		}
		if ($seen == 1) {
			$count++;
		}
		if ($count == 5) {
			#why doesnt the regex /([A-Z]{1}[a-z]{2} +[^<>]+/) work? Should grab everything with a capital, 2 lower case letters, one or more spaces followed by anything until the closing <. Should grab for example: Tue 13:00 - 15:00 but it doesnt... 		
			#print "$line";
			if ($line =~ /([A-Z]{1}[a-z]{2} [0-9]{2}:[0-9]{2}[^<>]+)/) {
				print "$course: $semester $1\n";
				$lecture_codes{$course}{$semester} = $1;
				#$seen = 0;
				$count = 0;
			}
			$seen = 0;
		}
	}
}
#for $coursecode (keys %lecture_codes) {
#	print "$coursecode: ";
#	for $semestervalue (keys %{ $lecture_codes{$coursecode} } ) {
#		print "$semestervalue ";
#		print "$lecture_codes{$coursecode}{$semestervalue}";
#	}
#	print "\n";
#}
#print "Key: $_ and Value: $lecture_codes{$_}{$_}\n" foreach (keys%lecture_codes);
#}
