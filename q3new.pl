#!/usr/bin/perl -w

$max = 0;
foreach $arg (@ARGV) {
	$seen{$arg}++;
	if ($seen{$arg} > $max) {
		$max = $seen{$arg};
	}
}

foreach $curr (@ARGV) {
	if ($seen{$curr} == $max) {
		print $curr."\n";
		last;
	}
}
