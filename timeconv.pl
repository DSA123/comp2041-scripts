#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	$before = $line;
	$time = $line;
	$after = $line;
	$before =~ s/ [0-9]{2}:.*//;
	$after =~ s/.+:[0-9]{2} //;
	$time =~ s/.+ [0-9]{1,2} //;
	$time =~ s/ [A-Z]{3}.*//;
	@times = split ':', $time;
	$hours = $times[0];
	$mins = $times[1];
	$secs = $times[2];
	
	if ($hours >= 12) {
		$secs = $secs."pm";
	} else {
		$secs = $secs."am";
	}
	
	if ($hours > 12) {
		$hours -= 12;
	} elsif ($hours == 0) {
		$hours += 12;
	}
	$time = $hours.":".$mins.":".$secs;
	
	$final = $before." ".$time." ".$after;
	push @lines, $final;	
}

print "\n";
foreach $var (@lines) {
	print $var."\n";
}
