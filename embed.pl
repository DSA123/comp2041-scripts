#!/usr/bin/perl -w

$command = 0;
$path = 0;
while ($line = <STDIN>) {
	chomp($line);
	@strings = split '\s+', $line;
	push @strings, "\n";	
	foreach $text (@strings) {
		if ($text =~ /^\<|\>$/) {
			if ($text =~ /^\<\!/) {
				$command = 1;
				$path = 0; 
			} elsif ($text =~ /^\</) {
				$path = 1;
				$command = 0;
			}
			if ($command == 1) {
				$text = "command";
			} elsif ($path == 1) {
				open($fh, '<', $text) or die $!;
				$temp = <$fh>;
				close $fh;
				$text = $temp;
			}
			if ($text =~ /\>$/) {
				$command = 0;
				$path = 0;
			} 
		}
		push @process, $text;
	}
}

push @all, @process;

$seen = 0;
foreach $string (@all) {
	if ($string =~ /\n/) {
		$seen = 1;
	}
	if ($seen == 0) {
		print $string." ";
	} else {
		print $string;
	}
	$seen = 0;
}
