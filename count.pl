#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@words = split '[^a-zA-Z]', $line;
	push @total, @words;
}

$max = 0;

foreach $word (@total) {
	$count{$word}++;
	if ($count{$word} >= $max) {
		$max = $count{$word};
		$maxname = $word;
	}
}

foreach $key (keys %count) {
	print $key." ".$count{$key}."\n";
}

print "\n";
print "Word with the highest count is: ".$maxname." with a count of ".$max."\n";

%seen = ();

foreach $val (sort values %count) {
	foreach $key (keys %count) {
		if (($count{$key} == $val) && !(defined $seen{$key})) {
			$name = $key;
			$seen{$name}++;
			last;
		} else {
			$name = "test name";
		}
	}
	print "Word: ".$name." Count: ".$val."\n";
}
