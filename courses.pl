#!/usr/bin/perl -w

$course = $ARGV[0];
$courseurl = $course."KENS";

@page_content = `wget -q -O- http://www.timetable.unsw.edu.au/current/$courseurl.html`;

foreach $code (@page_content) {
	if ($code =~ /($course[0-9]{4})/) {
		if (! exists $course_codes{$1}) {
			$course_codes{$1} = $1;
		}
	}
}
print "$_\n" for sort keys %course_codes;
