#!/bin/bash

for file in *
do
	if [[ "$file" =~ \.jpg$ ]]
	then
		png=`echo "$file" | sed 's/[.]jpg$/.png/g'`
		if test -e "$png"
		then
			echo ""$png" already exists"
			exit 1
		else 
			#echo "About to convert $file"
			#echo ""$png" already exists"
			convert "$file" "$png" && rm "$file"
		fi
	fi
done 
