#!/usr/bin/perl -w

while ($line = <STDIN>) {
	$line =~ tr/aeiou|AEIOU/AEIOU|aeiou/;
	print $line;
}
