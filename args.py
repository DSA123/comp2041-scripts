#!/usr/bin/python2.7 -u
import sys
# A simple shell script demonstrating access to arguments.
# written by andrewt@cse.unsw.edu.au as a COMP2041 example

print 'My', 'name', 'is', sys.argv[0]
#echo My process number is $$
print 'I', 'have', #, 'arguments'
print 'My', 'arguments', 'separately', 'are', *
print 'My', 'arguments', 'together', 'are', "@"
print 'My', '5th', 'argument', 'is', sys.argv["'5'"]
