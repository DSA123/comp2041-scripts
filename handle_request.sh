#!/bin/sh
#Run from webserver.sh to handle a single http request
# written by andrewt@cse.unsw.edu.au as a COMP2041 example

read http_request || exit 1

status_line="HTTP/1.0 200 OK"
content_type="text/plain"
content="Hi, I am a shell webserver and I received this HTTP request: $http_request"
content_length=`echo "$content"|wc -c`
filename=`echo "$http_request" | cut -d" " -f2 | sed -e 's/[/]$//g' -e 's/^[/]//g'`
echo "file is $filename" 
if [ -f "public_html/$filename" ]; then
  	echo "HTTP/1.1 200 OK"
	#file -bi "$filename"
	#echo "Content-Type: `/usr/bin/file -bi \"$filename\"`"
  	#echo "Content-Type: `open "public_html/$filename"`"
	open "public_html/$filename"
  	echo ""
	echo "----------------------------------------------------------------------------------------------"
	echo ""
elif [ -d "public_html/$filename" ]; then
        content_type="text/html"
	echo "Content-Type: "$content_type""
	found=false
	for file in public_html/$filename/*
	do
		fixedFile=`echo "$file" | sed 's,//,/,g'`
		#echo "$fixedFile"
		currentfile=`echo "$file" | egrep -o "[/][^/]*$" | cut -d"/" -f2`
		echo "$currentfile"
		if [[ "$currentfile" == "index.html" ]]
		then
			found=true
			open "$file"
			break
		fi
	done
	if [ "$found" = false ]; then
		for file in public_html/$filename/*
		do
			fixedFile=`echo "$file" | sed 's,//,/,g'`
                	#echo "$fixedFile"
			currentfile=`echo "$file" | egrep -o "[/][^/]*$" | cut -d"/" -f2`
			echo "<a href="localhost:2041/"$fixedFile"">"$currentfile"</a>"
			# if link clicked then do open "$fixedFile"
			echo "Generating link for: $currentfile"
		done
	fi
else
  	echo "HTTP/1.1 404 Not Found: The request resource was not found"
  	echo "Content-Type: text/html"
  	echo "-----------------------------------------------"
fi
echo "HTTP/1.0 200 OK"
echo "Content-type: $content_type"
echo "Content-length: $content_length"
echo
echo "$content"
exit 0
