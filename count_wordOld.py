#!/usr/bin/python -u

import sys, re, fileinput

wordgiven = sys.argv[0]

words = []
#found = []
for line in sys.stdin:
	line = re.sub('[^a-zA-Z ]', ' ', line)
        line = re.sub(' +', ' ', line)
        #found = line.split()
	#words.append(found)
	found = re.findall(r"([a-zA-Z]+)", line)
	found = [ i.strip().split(' ') for i in found ]
	words.append(found)
	#print "%s" %found
#print "%s" % words
foundwords = 0
for element in words:
	#foundwords += 1
	eupper = element.upper()
	elower = element.lower()
	wordupper = wordgiven.upper()
	wordlower = wordgiven.lower()
	if ((eupper is wordupper) or (elower is wordlower)):
		foundwords += 1	
	#print "%s" % element
print "%s occurred %s times" % wordlower % foundwords
