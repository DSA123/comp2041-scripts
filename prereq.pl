#!/usr/bin/perl -w

@args = @ARGV;
@visited = ();
$origcourse = $ARGV[1];
#print "$origcourse";
if (@ARGV == 2) {
	if ($ARGV[0] eq "-r") {
		while (@args) {
			$course = pop @args;
			$found = 0;
			foreach $f (@visited) {
				if ($f eq $course) {
					$found = 1;
					last;
				}
			}	
			if ($found == 0) {
				push @visited, $course;
			} else {
				next;
			}
			#print "course is $course\n";
			if ($course ne "-r") {
			$ugurl = "http://www.handbook.unsw.edu.au/undergraduate/courses/2015/$course.html";
			$pgurl = "http://www.handbook.unsw.edu.au/postgraduate/courses/2015/$course.html";	
			$ugurlcontents = `wget -q -O- $ugurl`;
			$pgurlcontents = `wget -q -O- $pgurl`;	
			if ($ugurlcontents) {
				#$ugurlcontents =~ /(Pre-?requisites?:.*)/i;	
				#print "before test\n";
				$ugurlcontents = `wget -q -O- $ugurl | egrep -o -i "pre-?requisites?:? ?[^<]+"`; 
				#print "$ugurlcontents\n";
				if ($ugurlcontents =~ /(pre-?requisites?:? ?.*)/i) { 
					$ugprereq = $1;
					#print "ugprereq: $ugprereq\n";
					@ugcourses = ($ugprereq =~ /[A-Z]{4}[0-9]{4}/gi);
				} else {
					@ugcourses = ();
				}
			}
			if ($pgurlcontents) {	
				#$pgurlcontents =~ /(Pre-?requisites?:.*)/i;
				$pgurlcontents = `wget -q -O- $pgurl | egrep -oi "Pre-?requisite[s]?:[^<]+"`;
				if ($pgurlcontents =~ /(Pre-?requisites?:.*)/i) {
					$pgprereq = $1;
					#print "UG Courses: @ugcourses\n";
					@pgcourses = ($pgprereq =~ /[A-Z]{4}[0-9]{4}/gi);
                        		#print "PG Courses: @pgcourses\n";
		 		} else {
					@pgcourses = ();
				}
			}
			if (@ugcourses) {
				if (@pgcourses) {
					%duplicates = ();
                        		@allcourses = grep ( !$duplicates{$_}++, @ugcourses, @pgcourses);
				} else {
					@allcourses = @ugcourses;
				}
			} elsif (@pgcourses) {
				@allcourses = @pgcourses;
			} else {
				@allcourses = ();
			}

			@seen{ @visited } = ();
			foreach (@allcourses) {
   	 			push(@unseencourses, $_) unless exists $seen{$_};
			};

                        @seen1{ @args } = ();
                        foreach (@unseencourses) {
                                push(@unseenargs, $_) unless exists $seen1{$_};
                        };
			
			@args = @unseenargs;
			
			@storedvisited = @visited;
			} else {
				#print "Visited: @visited\n";
				last;
			}
		}
		foreach $f (@storedvisited) {
			$f = uc $f;
		}
		@sortvisited = sort @storedvisited;
		foreach $f (@sortvisited) {
			if ($f eq $origcourse) {
				next;
			}
			print "$f\n";
		} 
		#print "@sortvisited\n";
	}
} else {
	$course = $ARGV[0];
	$pgurl = "http://www.handbook.unsw.edu.au/postgraduate/courses/2015/$course.html";
	$ugurl = "http://www.handbook.unsw.edu.au/undergraduate/courses/2015/$course.html";
	$ugurlcontents = `wget -q -O- $ugurl | egrep -o "Prerequisite[s]?:[^<]+" | egrep -o "[A-Z]{4}[0-9]{4}"`;
	$pgurlcontents = `wget -q -O- $pgurl | egrep -o "Prerequisite[s]?:[^<]+" | egrep -o "[A-Z]{4}[0-9]{4}"`;
	#print "contents is $ugurlcontents$pgurlcontents";
	#print "args is @args";
	#print "$pgurlcontents";
	$allcontents = $ugurlcontents.$pgurlcontents;
	@contents = $allcontents;
	@uniquecontents = do {my %seen3; grep { !$seen3{$_}++ } @contents};
	print "@uniquecontents";	
}
