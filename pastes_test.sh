#!/usr/bin/perl -w

@files = @ARGV;
print "Test files are @files\n";
print "Running /usr/bin/paste -s @files\n";
`/usr/bin/paste -s @files >output1`;
print "Running ./pastes.pl @files\n";
`./pastes.pl @files >output2`;
print "Running diff on the output\n";
#print "binpaste is $binpaste and pastes is $pastes";
$diff = `diff output1 output2`;
if ($diff eq "") {
	print "Test succeeded - output of ./pastes.pl matched /usr/bin/paste\n";
} else {
	print "Test failed - output of ./pastes.pl does not match /usr/bin/paste\n";
}
#print "diff is $diff\n";
#print "@files\n";

