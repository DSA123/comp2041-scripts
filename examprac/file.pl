#!/usr/bin/perl -w

open $fh, "<", "badwords.txt" or die "cannot open $!\n";
while ($line = <$fh>) {
	chomp($line);
	@text = split '[^a-zA-Z]+', $line;
	push @badwords, @text;
}
close $fh;

print (join " ", @badwords);
print "\n";


foreach $file (glob("*")) {
	open $fh, "<", $file or die "cannot open $!\n";
	$seen = 0;
	while ($line = <$fh>) {
		chomp($line);
		foreach $word (@badwords) {
			if ($line =~ /\b$word\b/) {
				$seen = 1;
				last;
			}
		}
		if ($seen == 1) {
			print $file." contains badwords\n";
			last;
		}
	}
	close $fh;
	print "checked ".$file." for badwords\n";
}			
