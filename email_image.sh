#!/bin/bash

if [[ $# -eq 0 ]]
then
	echo "Usage: <image> <image> ..."
	exit 1	
else
	for var in "$@"
	do
		#echo "$var"
		display "$var"
		#echo -n "Please enter a valid email address and press [ENTER]: " &>/dev/null
		read -p "Enter an email address and press [ENTER]: " address
		if [[ "$address"  =~ ^.*@.*$ ]]
		then
			
			#echo -n "Please enter a message and press [ENTER]: "
			read -p "Please enter a message and press [ENTER]: "  message
			echo "$message" | mutt -s "`echo "$var" | cut -d"." -f1`!" -a "$var" -- "$address"
			echo "Address to email this image to? "$address""
			echo "Message to accompany image? "$message""
			echo ""$var" sent to "$address""
		fi
	done
fi	
