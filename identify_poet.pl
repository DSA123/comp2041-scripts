#!/usr/bin/perl -w
#trash comment to allow git commit to test autotests
#$wordgiven = $ARGV[0];

foreach $poem_file (@ARGV) {
	open ($ph, $poem_file) or die;
	@poem_words = ();
	while (<$ph>) {
		$_ =~ s/[^a-zA-Z ]/ /g;
		$_ =~ s/ +/ /g;
		@poemfile_words = grep /([a-zA-Z]+)/, split ' ', $_;
		push @poem_words, @poemfile_words;
		#print "words in poem are @poem_words\n";
        }
	#print "poem words are now @poem_words\n";
	foreach $file (glob "poets/*.txt") {
		#print "$file\n";
		open($fh, $file) or die;
		while (<$fh>) {
			$_ =~ s/[^a-zA-Z ]/ /g;
			$_ =~ s/ +/ /g;
			@found = grep /([a-zA-Z]+)/, split ' ', $_;
			#$count += @found;
			#print "Found $count1\n";
			push @words, @found;
			#print "words found are: @found\n";
		}
		#print "poem words are now @poem_words\n";
		foreach $wordgiven (@poem_words) {
			$found = 0;
			foreach $f (@words) {
				$fupper = uc $f;
				$flower = lc $f;
				$wordupper = uc $wordgiven;
				$wordlower = lc $wordgiven;
				if (("$fupper" eq "$wordupper") || ("$flower" eq "$wordlower")) {
					#print "found $wordlower and $wordgiven match\n";
					$found++;
				}
			}
			$total_words = scalar @words;
			@file_name = grep /([a-zA-Z]+)/, split '[^a-zA-Z]+', $file;
			shift @file_name;
			pop @file_name;
			#$freq_number = $found / $total_words;
			$log_number = log(($found+1)/$total_words);
			$total_log += $log_number;
			$file_name_string = join(" ", @file_name);
			#print "$file: log_probability of $total_log for $file_name_string\n";
			#print "word is $wordgiven, file is $file_name_string, poem is $poem_file\n";
			#printf "log((%d+1)/%6d) = %8.4f %s\n", $found, $total_words, $log_number, $file_name_string;
			#printf "%4d/%6d = %.9f %s\n", $found, $total_words, $freq_number, $file_name_string;
			#print "$found / $total_words = test number ... @file_name\n";
			#print "$wordlower occurred $found times\n";
		}
			$total_log = sprintf "%.1f", $total_log;
			push @poet_total_logs,"$poem_file: log_probability of $total_log for $file_name_string\n";
			#print join "",@poet_total_logs, "\n";
			$found = 0;
			@found = ();
			@words = ();
			@file_name = ();
			$total_log = 0;
	}
	@sorted_poet_totals = sort {$a cmp $b} @poet_total_logs;
	print join "", @sorted_poet_totals;
	#print join "",@poet_total_logs;
	$identified_name = $sorted_poet_totals[0];
	$identified_log = $sorted_poet_totals[0];
	$identified_name =~ s/[^A-Z]+//;
	chomp($identified_name);
	$identified_log =~ s/[^-0-9.]//g;
	$identified_log =~ s/[0-9]{1}.//;
	
	#3 lines below are fix for cse linux version
	#$identified_name =~ s/[^A-Z]+//;
        #chomp($identified_name);
        #($identified_log) = $identified_log =~ /(-[0-9]+.[0-9]+)/;
	
	printf "$poem_file most resembles the work of $identified_name (log-probability=$identified_log)\n";
}
