#!/usr/bin/python
import re, sys
L = []
whalelist = []
for line in sys.stdin:
	line = line.lower()
	line = re.sub(' +', ' ', line)
	line = re.sub(r's$', '', line)
	#print "%s" % line
	L.append(line)
	#print "Convert data:"
	#print "Printing lines:"
	#print line,
#print "Printing whales!"
while (L):
	line = L.pop()
	pods = 1
	individuals = 0
	#print "%s" % line
	species1 = str(re.search(r"([a-z \']+)", line).group())
	species1 = re.sub('^ ', '', species1)
	species1 = re.sub(' +', ' ', species1)
	species1 = re.sub(' $', '', species1)
	number = int(re.search('^[0-9]+', line).group())
	individuals = individuals + number
	for whales in L:
		species2 = str(re.search(r"([a-z \']+)", whales).group())
		species2 = re.sub('^ ', '', species2)
        	species2 = re.sub(' +', ' ', species2)
        	species2 = re.sub(' $', '', species2)
		if (species1 == species2):
			#print "Found match: %s" %whales
			number = int(re.search('^[0-9]+', whales).group())
			individuals = individuals + number
			pods = pods + 1
			L.remove(whales)
			#print "Found comparison: %s and %s" %(species, whales)
	for whales in L:
                species2 = str(re.search(r"([a-z \']+)", whales).group())
                species2 = re.sub('^ ', '', species2)
                species2 = re.sub(' +', ' ', species2)
                species2 = re.sub(' $', '', species2)
                if (species1 == species2):
                        #print "Found match: %s" %whales
                        number = int(re.search('^[0-9]+', whales).group())
                        individuals = individuals + number
                        pods = pods + 1
                        L.remove(whales)
                        #print "Found comparison: %s and %s" %(species, whales)
	podsstring = str(pods)
	indstring = str(individuals)
	#finalvar = str.join(species1, podsstring, indstring);
	finalvar = species1 + " " + podsstring + " " + indstring 
	#print "%s" %finalvar
	whalelist.append(finalvar)
	whalelist.sort()
for finalwhale in whalelist:
	finalspecies = str(re.search(r"([a-z \']+)", finalwhale).group())
	finalspecies = re.sub(' $', '', finalspecies)
	finalpods = int(re.search(' [0-9]+ ', finalwhale).group())
	finalindiv = int(re.search('[0-9]+$', finalwhale).group())
	print "%s observations: %d pods, %d individuals" % (finalspecies,finalpods,finalindiv)
	#FL.append(finalvar)
	#for whale in FL:
		#print whale
	#print "%s" % finalvar
	#print "%s observations: %d pods, %d individuals" %(species1,pods,individuals)
#for line in L: print line,
