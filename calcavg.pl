#!/usr/bin/perl -w

if ($#ARGV < 0) {
	print "Usage: ex3 Course\n";
} else {
	$given = $ARGV[0];
	while ($line = <STDIN>) {
		chomp ($line);
		$line =~ /^[0-9]+\s+([A-Z]{4}[0-9]{4})\s+([0-9]{4})\s+([A-Z]{1}[0-9])\s+([0-9]+).*$/;
		$course = $1;
		$year = $2;
		$session = $3;
		$mark = $4;
		if ($course =~ /$given/) {
			$totalmarks{$year}{$session} += $mark;
			$countmarks{$year}{$session}++;
		}
	}
	
	foreach $curryear (sort keys %totalmarks) {
		foreach $currsession (keys %{$totalmarks{$curryear}}) {
			#$average = $totalmarks{$curryear}{$currsession} / $countmarks{$curryear}{$currsession};
			#$average = sprintf("%.1f", $average);
			$average = average($totalmarks{$curryear}{$currsession}, $countmarks{$curryear}{$currsession});
			push @lines, "$given $curryear $currsession $average";
		}
	}
	if (@lines) {
		print join("\n", @lines)."\n";
	} else {
		print "No marks for course $given\n";
	}
}

sub average {
	my ($total, $count) = @_;
	$average = $total / $count;
	$average = sprintf("%.1f", $average);
	return $average;
}
