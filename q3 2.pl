#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@words = split ' ', $line;
	
	foreach $word (@words) {
		if ($word =~ /[0-9]+.[0-9]+/) {
			$before = $word;
			$after = $word;
			$before =~ s/[0-9].*//;
			$after =~ s/.+[0-9]//;
			$word =~ s/^[^0-9]+//;
			$word =~ s/[^0-9.]+$//;
			push @nonfinal, $word;
			$word = int($word);
			$word = $before.$word.$after;
		}
			push @final, $word;
	}
	push @final, "\n";
}

$count = 0;
foreach $text (@final) {
	if ($text =~ /\n/) {
		$count = -1;
	}
	if ($count != 0) {
		$text = " ".$text;
	}
	print $text;
	$count++;
}

print join(" ", @nonfinal)."\n";
