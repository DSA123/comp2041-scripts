#!/usr/bin/perl -w

foreach $arg (@ARGV) {
	$seen{$arg}++;
}

$max = 0;
$prevcount = 0;
foreach $current (keys %seen) {
	$count = $seen{$current};
	if ($count >= $prevcount) {
		$max = $count;
		#push @dupes, $current;
	}
	$prevcount = $count;
}

foreach $current (keys %seen) {
	if ($seen{$current} == $max) {
		push @dupes, $current;
	}
}


$printed = 0;
foreach $var (@ARGV) {
	if ($printed == 0) {
		foreach $dupe (@dupes) {
			if ($var eq $dupe) {
				print $dupe."\n";
				$printed = 1;
				last;
			}
		}
	}
}
