#!/usr/bin/perl -w


foreach $num (@ARGV) {
	$freq{$num}++;
}

$max = 0;
foreach $key (keys %freq) {
	$weight{$key} = ($freq{$key} * $key);
	if ($weight{$key} >= $max) {
		$max = $weight{$key};
		$heaviest = $key;
	}
}

print $heaviest."\n";
