#!/usr/bin/perl -w

# written by andrewt@cse.unsw.edu.au August 2015
# as a starting point for COMP2041/9041 assignment 
# http://cgi.cse.unsw.edu.au/~cs2041/assignment/shpy
%imports = ();
$indent = 0; #false
$test_boolean = 0; #false
$test_r_seen = 0; #false
$test_d_seen = 0; #false
$expr_seen = 0; #false
$echo_noption = 0; #false

while ($line = <>) {
	if ($line =~ /^cd *|\s+test\s+-r/) {
                if (!exists($imports{"os"})) {
                        $imports{"os"} = "os";
                }
        }
	if ($line =~ /^ls *|^pwd *|^id *|^date */) {
		if (!exists($imports{"subprocess"})) { 
			$imports{"subprocess"} =  "subprocess";
		}
	}
	if ($line =~ /\s*exit\s*|\s*read\s*|\$(\d+|@)\s*/) {
		if (!exists($imports{"sys"})) {
			$imports{"sys"} = "sys";
		}
	}
	if ($line =~ /\*\..+/) {
		if (!exists($imports{"glob"})) {
			$imports{"glob"} = "glob";
		}
	}
	if ($line =~ /\s+test\s+-d/) {
		if (!exists($imports{"os.path"})) {
                        $imports{"os.path"} = "os.path";
                }
	}
	chomp $line;
	push @stored_lines, $line;
}
$line_count = 0;
foreach $line (@stored_lines) {
    if ($line =~ /^#!/ && $line_count == 0) {
	print "#!/usr/bin/python2.7 -u\n";
	for (sort keys %imports) {
        	print "import $imports{$_}\n";
	}
    } elsif ($line =~ /^#/) {
		print "$line\n";
    } elsif ($line =~ /\s*cd\s+/) {
	@cdcontents = split ' ', $line;
	shift @cdcontents;
	foreach $text (@cdcontents) {
		if ($indent == 1) {
			print "\t";
		}
		print "os.chdir(\'$text\')\n";
	}
    } elsif ($line =~ /\s*echo\s+/) {
	@words = grep /(.+)/, split ' ', $line;
        shift @words;
	if ($indent == 1) {
		print "\t";
	}
	print "print ";
	if ($words[0] =~ /-n/) {
		$echo_noption = 1;	
	}
	if ($words[0] =~ /'.+/) {
		print join(' ',@words);
	} elsif ($words[0] =~ /".+/) {
		print join(" ", @words);
	} else {
		$i = 0;
		foreach $text (@words) {
			if ($text =~ /\$[0-9]+\s*/) {
				$text =~ s/\$//;
				print "sys.argv[$text]";
			} elsif ($text =~ /\$.+/) {
				$text =~ s/\$//;
				print "$text";
			} else {
				print "\'$text\'";
			}
			if ($i < $#words) {
				print ", ";
			}
			$i++;
		}
	}
	print "\n" if $echo_noption == 0;
	#print "print ";
	#print "'",join ( "', '", @words),"'\n";
    } elsif ($line =~ /\s*for\s+/) {
	$indent = 1;
	@forloop = split ' ', $line;
	$count  = 0;
	foreach $text (@forloop) {
		#first 3 words are "for blah in"	
		if ($count < 3) {
			print "$text ";
		} else {
			if ($text =~ /[0-9]+/) {
				print "$text";
			} elsif ($text =~ /\*\..+/) {
				print 'sorted(glob.glob("'."$text".'"))';
			} else {
				print "\'$text\'";
			}
			if ($count < $#forloop) {
				print ", ";
			} else {
				print ":\n";
			}
		}
	$count++;
	}	  
    } elsif ($line =~ /\s*if\s+|\s*elif\s+|\s*else$|\s*while\s+/) {
	$indent = 1;
	if ($line =~ /\s+test\s+/) {
		if ($line =~ /-(le)/) {
			$test_int = 1;
		} else {
			$test_int = 0;
		}
	}
        @if_words = split ' ', $line;
	$if_count = 0;
        foreach $text (@if_words) { 
                if ($if_count < 1 && $text !~ /test/ || $text !~ /[a-zA-Z0-9]+/) {
                        if ($text =~ /=/) {
				print "==";
			} else {
				print "$text";
			}
			if ($if_count < $#if_words) {
                        	print " ";
                	} else {
				if ($test_r_seen == 1) {
					print ", os.R_OK)";
				} elsif ($test_d_seen == 1) {
					print ")";
				}
                        	print ":\n";
                	}
                } else {
                        if ($text !~ /test/) {
				if ($test_boolean == 1) {
					if ($text =~ /-r/) {
						print "os.access(";
						$test_r_seen = 1;
					} elsif ($text =~ /-d/) {
						print "os.path.isdir(";
						$test_d_seen = 1;
					} elsif ($text =~ /-le/) {
						print "<= ";
					} elsif ($text =~ /-lt/) {
						print "< ";
					} elsif ($text =~ /-ge/) {
						print ">= ";
					} elsif ($text =~ /-gt/) {
						print "> ";
					} elsif ($text =~ /-ne/) {
						print "!= ";
					} elsif ($text =~ /\$.+/) {
						$text =~ s/\$//;
						if ($test_int == 1) {
							print "int(".$text.")";
						} else {
							print "$text";
						}
						if ($if_count < $#if_words) {
                                                        print " ";
                                                } else {
							print ":\n";
						}
					} else {
						print "\'$text\'";
                                        	if ($if_count < $#if_words) {
                                                	print " ";
                                        	} else {
                                                	if ($test_r_seen == 1) {
                                        			print ", os.R_OK)";
                                			} elsif ($test_d_seen == 1) {
                                       	 			print ")";
                               		 		}
							print ":\n";
                                        	}
					}
				} else {
					print "\'$text\'";
					if ($if_count < $#if_words) {
                        			print " ";
                			} else {
						if ($test_r_seen == 1) {
                                        		print ", os.R_OK)";
                                		} elsif ($test_d_seen == 1) {
                                        		print ")";
                                		}
                        			print ":\n";
					}
				}
				#$test_boolean = 0;
			} else {
				$test_boolean = 1;
			}
                }
	$if_count++;
	}
    } elsif ($line =~ /\s*ls\s*|\s*pwd\s*-?[a-zA-Z]?|\s*id\s*-?[a-zA-Z]?|\s*date\s*[-+"]?/) {
	@commands = grep /(.+)/, split ' ', $line;
	if ($indent == 1) {
		print "\t";
	}
	print "subprocess.call([";
	$j = 0;
	foreach $text (@commands) {
		if ($j == 0) {
			if ($text =~ /\$@/) {
				print "] + sys.argv[1:";
				last;
			} else {
				print "\'$text\'";
			}
		} else {
			if ($text =~ /\$@/) {
                                print "] + sys.argv[1:";
                                last;
                        } else {
				print ", \'$text\'";
			}
		}
		$j++;
	}
	print "])";
	print "\n";
    } elsif ($line =~ /[a-zA-Z_]+[a-zA-Z0-9_]*=.+/) {
        @variables = split /=/, $line;
	$varcontents = pop @variables;
	$varname = pop @variables;
        $varname =~ s/^\s+//;
	$varname =~ s/\s+$//;
	$varcontents =~ s/^\s+//;
	$varcontents =~ s/\s+$//;
	if ($indent == 1) {
                print "\t";
        }
	if ($varcontents =~ /^`(.+)`/) {
		$varcontents =~ s/^`//;
		@var_backtick_contents = split /`/, $varcontents;
		#print "$var_backtick_contents[0]\n";	
		#$varcontents =~ $1;
		#$varcontents =~ s/^`//;
                #$varcontents =~ s/`$//;
                $varcontents = $var_backtick_contents[0];
		$remainder = $var_backtick_contents[1] if defined $var_backtick_contents[1];
		$varcontents =~ s/^\s+//;
		$varcontents =~ s/\s+$//;
		$remainder =~ s/^\s+// if defined $remainder;
                $remainder =~ s/\s+$// if defined $remainder;
		@var_backtick = split / /, $varcontents;
                $var_backtick_count = 0;
                print "$varname = ";
		foreach $text (@var_backtick) {
                        if ($text !~ /expr/) {
                                if ($text =~ /\$.+/) {
                                        $text =~ s/^\$//;
					if ($expr_seen == 1) {
						print "int(".$text.")";
					} else {
						print "$text";
					}
                                } else {
                                        print "$text";
                                }       
                        	if ($var_backtick_count < $#var_backtick) {
                                	print " ";
                        	}
			} else {
				$expr_seen = 1;
			}
			$var_backtick_count++;       
                }
		print " $remainder" if defined $remainder;
		print "\n";       
	} elsif ($varcontents =~ /\$\d+/) {
		$varcontents =~ s/\$//;
		$varcontents = "sys.argv[".$varcontents."]";
		print "$varname = $varcontents\n";
	} elsif ($varcontents =~ /\$\w+|^\d+$/) {
		$varcontents =~ s/\$//;
		print "$varname = $varcontents\n";	
	} else {
		print "$varname = \'$varcontents\'\n";
	}    		
    } elsif ($line =~ /\s*read\s+/) {
	if ($indent == 1) { 
		print "\t";
	}
	@read_line = split / /, $line;
	$read_name = pop @read_line;
	print "$read_name = sys.stdin.readline().rstrip()\n";
    } elsif ($line =~ /\s*exit\s+/) {
	if ($indent == 1) {
		print "\t";
	}
	@exitnumber = grep /(.+)/, split ' ', $line;
	$number = pop @exitnumber;
	print "sys.exit($number)\n";
    } elsif ($line =~ /\s*do\s*|\s*then\s*/) {
	#print "";
	$indent = 1;
    } elsif ($line =~ /\s*done$|\s*fi$/) {
	$indent = 0;
    } elsif ($line =~ /^$/) {
	print "\n";
    } else {
        # Lines we can't translate are turned into comments
        print "#$line\n";
    }
$line_count++;
}
