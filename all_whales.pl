#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp ($line);
        if ($line =~ /(^.*s$)/i) {
		$line =~ s/s$//i;
	}
	$line =~ s/^ +//;
	$line =~ s/ +$//;
	$line =~ s/ +/ /;
	$line = lc $line;
	push @whales, $line;
}
#$pods = 1;
#$individuals = 0;
while (@whales) {
	$pods = 1;
	$individuals = 0;	
	$whale = pop @whales;
	if (defined $whale) {
		$species1 = `echo "$whale" | egrep -o "[^0-9]+"`;
		$species1 =~ s/^ +//;
		$species1 =~ s/ +$//;
		$species1 =~ s/ +/ /;
		chomp ($species1);
		$individuals = `echo "$whale" | egrep -o "[0-9]+"`;
		chomp ($individuals);
		$count = 0;
		foreach $f (@whales) {
			if (defined $f) { 
				$species2 = `echo "$f" | egrep -o "[^0-9]+"`;
				$species2 =~ s/^ +//;
				$species2 =~ s/ +$//;
				$species2 =~ s/ +/ /;
				chomp ($species2);
				#print "Comparing: $species1 and $species2!\n";
				if ($species2 eq $species1) {
					#print "species found: $species2\n";
					$individualsfound = `echo "$f" | egrep -o "[0-9]+"`;
					$pods++;
					$individuals = $individuals + $individualsfound;
					#undef $whales[$count];
					splice @whales, $count, 1;
					#splice @whales, $i, 1;
				}
			$count++;
			#print "$species1 observations: $pods pods, $individuals individuals\n";
			}
		}
		$count = 0;
		foreach $f (@whales) {
                        if (defined $f) {
                                $species2 = `echo "$f" | egrep -o "[^0-9]+"`;
                                $species2 =~ s/^ +//;
                                $species2 =~ s/ +$//;
                                $species2 =~ s/ +/ /;
                                chomp ($species2);
                                #print "Comparing: $species1 and $species2!\n";
                                if ($species2 eq $species1) {
                                        #print "species found: $species2\n";
                                        $individualsfound = `echo "$f" | egrep -o "[0-9]+"`;
                                        $pods++;
                                        $individuals = $individuals + $individualsfound;
                                        #undef $whales[$count];
                                        splice @whales, $count, 1;
                                        #splice @whales, $i, 1;
                                }
                        $count++;
                        #print "$species1 observations: $pods pods, $individuals individuals\n";
                        }
                }
		$finalwhale = join "&", $species1, $pods, $individuals;
		push @visited, $finalwhale;
	}
}
@sorted = sort(@visited);
foreach $f (@sorted) {
	($species, $pods, $individuals) = (split /&/, $f);
	print "$species observations:  $pods pods, $individuals individuals\n";
}	 
#print "@visited\n";
