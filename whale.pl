#!/usr/bin/perl -w
if (@ARGV == 1) {
	$name = $ARGV[0];
	while ($line = <STDIN>) {
		chomp ($line);
		push @whales, $line;
	}
	$pods = 0;
	$individuals = 0;
	foreach $f (@whales) {
		if (grep { /$name/ } $f) {
			$pods++;
			$number = `echo "$f" | egrep -o "[0-9]+"`;
			#print "number is $number\n";
			$individuals = $individuals + $number;
			#print "$name observations: $pods pods, $individuals individuals\n";
		}
	}
	print "$name observations: $pods pods, $individuals individuals\n";
} else {
	print "Usage: <species name> :ENTER: <data> :ENTER:\n";
}
