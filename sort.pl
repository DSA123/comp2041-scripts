#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp ($line);
	push @lines, $line;
}

@sorted = sort {$a <=> $b} @lines;
print "\n".join("\n", @sorted)."\n";
