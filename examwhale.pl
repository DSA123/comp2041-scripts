#!/usr/bin/perl -w

if ($#ARGV < 0) {
	print "Usage: ./examwhale.pl <species>\n";
} else {
	$name = $ARGV[0];
	while ($line = <STDIN>) {
		chomp($line);
		$species = $line;
		$number = $line;
		$number =~ s/ .*//;
		$species =~ s/^[0-9]+ //;
		$pods{$species}++;
		$individuals{$species} += $number;
	}
	print $name." observations: ".$pods{$name}." pods, ".$individuals{$name}." individuals\n";
}
