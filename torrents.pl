#!/usr/bin/perl -w

$i = 0;
@shows = ("Scandal", "Rizzoli", "Downton Abbey", "Walking Dead", "Good Wife", "Last Kingdom", "Ultimate Fighter", "Homeland", "Madam Secretary", "Grimm", "UFC");

print "Searching for ".join(',', sort @shows).":\n";
while ($i < 5) {
	$site = "https://torrentz.eu/verified?f=tv&p=$i";
	#$page = `wget -O- -q "$site"`;
	@page = `wget -O- -q "$site"`;
	push @contents, @page;
	$i++;
	print "."
}

$start = "<b>tv</b></dt><dd>";
foreach $line (@contents) {
	chomp ($line);
	if ($line =~ /$start/) {
		$line =~ s/\;.*//; 
		$line =~ s/.*\"\>?//;
		$line =~ s/\<.*//;
		$line =~ s/(s|S)[0-9]{2}(e|E)[0-9]{2}.*//;
		$line =~ s/(s|S)[0-9]{2}\s*.*//;
		$line =~ s/\s+[0-9]+x?[0-9]*.*//;
		$line =~ s/\s+$//;
		$found{$line}++;
	}
}


print "\nTV Shows Found:\n";
foreach $show (sort @shows) {
	foreach $text (sort keys %found) {
		#print $text."\n";
		if ($text =~ $show) {
			print "Found $show! ($text) \n";
			delete $found{$show};
			last;
		}
	}
}
