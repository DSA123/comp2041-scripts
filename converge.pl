#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	$line =~ s/\s+/ /g;
	@ints = split " ", $line;
	push @allints, @ints;
}

$converging = 0;
$count = 0;
foreach $int (@allints) {
	if (!defined $prevval) {
		$prevval = $int;
	}
	
	#print "Checking if ".$int." is less than ".$prevval."\n";
	if ($int < $prevval) {
		$diff = $prevval - $int;
		if (!defined $prevdiff) {
			$prevdiff = $diff;
		}
		#print "Checking if ".$prevdiff." is less than ".$diff."\n";
		if ($diff > $prevdiff) {
			$converging = 1;
			last;
		}
		$prevdiff = $diff;	
	} elsif ($int == $prevval) {
		$prevdiff = $diff;		
	} else {
		$converging = 1;
		last;
	}
	$count++;
	$prevval = $int;
}

if ($converging == 0) {
	print "converging\n";
} else {
	print "not converging\n";
}
