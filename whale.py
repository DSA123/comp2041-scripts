#!/usr/bin/python

import re, sys
species = sys.argv[1]
pods = 0
individuals = 0
for line in sys.stdin:
	if (re.search(species,line)):
		pods = pods + 1
		number = int(re.search('[0-9]+', line).group())
		individuals = individuals + number
		#print "%s" % number
print "%s observations: %d pods, %d individuals" % (species,pods,individuals)
