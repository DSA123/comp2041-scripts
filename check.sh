#!/bin/sh

#set -x
for file in *
do
	if test "$file" = "studentmarks.txt"
	then
		text=`cat "$file"`
		echo "$text" | cut -d" " -f1 | sort | uniq -c | egrep "^ +[2-9] +" | sed "s/ *[0-9] *//"
	fi 
done
