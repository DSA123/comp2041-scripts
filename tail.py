#!/usr/bin/python

import sys, re

for file in sys.argv[1:]:
	tail = 10
	numlines = sum(1 for line in open(file))
	skipped = numlines - tail
	#print tail
	#print numlines
	#print skipped
	count = 0
	for line in (open(file).readlines()):
		if (numlines < tail):
			print line,
		else:
			if (count >= skipped):
				print line,
				count = count + 1
			else:
				count = count + 1
