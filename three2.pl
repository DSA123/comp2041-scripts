#!/usr/bin/perl -w

$max = 0;

foreach $arg (@ARGV) {
	$count{$arg}++;
	if ($count{$arg} > $max) {
		$max = $count{$arg};
	}
}

foreach $arg (@ARGV) {
	if ($count{$arg} == $max) {
		print $arg."\n";
		last;
	}
}
