#!/usr/bin/python -u

import re, fileinput

count = 0
found = []
words = []
for line in fileinput.input():
	line = re.sub('[^a-zA-Z ]', ' ', line)
	line = re.sub(' +', ' ', line)
	found = re.findall(r"([a-zA-Z]+)", line)
	#print "Found array is currently: %s\n" % found
	#print "Found array size is: %s\n" % len(found)
	if (len(found) > 0):
		count += len(found)
	found = [i.strip().split(' ') for i in found]
	words.append(found)
#print "%s words" % count
#print "%s" %words
print "%s words" % count
