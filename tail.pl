#!/usr/bin/perl -w
@files = ();
$numLines = 10;
$numVars = @ARGV;
foreach $arg (@ARGV) {
        if ($arg eq "--version") {
                print "$0: version 0.1\n";
                exit(0);
        } elsif ($arg =~ /^-\d+$/) {
                $numLines = $arg;
                $numLines =~ s/-//g;
                if ( $numVars != 0 ) {
                        $numVars--;
                }
        } else {
                push @files, $arg;
        }
}
if ( @files ) {
	foreach $f (@files) {
                if ( open(F,"<$f") ) {
                        if ( $numVars > 1) {
                                print "==> $f <==\n";
                        }
			$j = `wc -l $f | egrep -o " [0-9]+ " | tr -d " "  | tr -d "\n"`;
			$skipped = $j - $numLines;
			$count = 0;
			#print "$j $skipped";
			@line = <F>;
			for ($line_number = $#line; $line_number >= 0; $line_number--) {
				if ($count >= $skipped) {
					print $line[$count];
				}
				$count++;	
			}
                        close(F);
                } else {
                        warn "$0: Can't open $f\n";
                }
        }
} else {
	$tempfile = 'tempfile.txt';
        open F, '>', $tempfile or die $!;
        while (<STDIN>) {
                last if /^$/;
                print F $_;
        }
        close F;
	open G, '<', $tempfile or die $!;
		$j = `wc -l $tempfile | egrep -o " [0-9]+ " | tr -d " "  | tr -d "\n"`;
        	$skipped = $j - $numLines;
        	$count = 0;      	
                @line = <G>;
                for ($line_number = $#line; $line_number >= 0; $line_number--) {
                	if ($count >= $skipped) {
                        	print $line[$count];
                        }
                        $count++;
                 }
        `rm tempfile.txt`
}
