#!/usr/bin/perl -w

foreach $f (@ARGV) {
	$i = 1;
	$output = `cat $f`;
	$newlines = `wc -l $f | egrep -o "^ +[0-9]+ +" | tr -d " "`;
	#print "$newlines";
	while ($i < $newlines) {
		$output =~ s/\n/\t/;
		$i++;
	}
	print "$output";
}
