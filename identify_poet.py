#!/usr/bin/python -u

import os, re, glob, sys, math

#wordgiven = sys.argv[1]
totallog = 0
poet_total_logs = []

for poemfile in sys.argv[1:]:
	with open(poemfile) as ph:
		poem_words = []
		poemlines = ph.readlines()
		for poemline in poemlines:
			poemline = re.sub('[^a-zA-Z ]', ' ', poemline)
                	poemline = re.sub(' +', ' ', poemline)
                	for word in poemline.split():
                		poem_words.append(word)
	for file in glob.glob("poets/*.txt"):
		with open(file) as f:
			lines = f.readlines()
			found = []
			for line in lines:
				line = re.sub('[^a-zA-Z ]', ' ', line)
				line = re.sub(' +', ' ', line)
				for word in line.split():
					found.append(word)
			#print "%s" % found
			#print "%d" %len(found)
		for wordgiven in poem_words:
			count = 0
			for element in found:
				eupper = element.upper()
				elower = element.lower()
				wordupper = wordgiven.upper()
				wordlower = wordgiven.lower()
				#print "comparing %s with %s and %s with %s" %(eupper, wordupper, elower, wordlower)
				if (eupper == wordupper or elower == wordlower):
					#print "match found"
					count += 1
			poetname = re.sub(".*/", "", file)
			poetname = re.sub("\..*", "", poetname)
			poetname = re.sub("_", " ", poetname)
			#decimal = (count / (len(found) * 1.0))
			#print "%4d/%6d = %.9f %s" %(count, len(found), decimal, poetname)
			lognum = ((count+1) / (len(found) * 1.0))
			log = math.log(lognum)
			totallog += log
			#print "log((%d+1)/%6d) = %8.4f %s" %(count, len(found), log, poetname)
		totallog = float(totallog)
		#print "%.1f" %totallog
		finalstring = "%s: log-probability of %.1f for %s" %(poemfile, totallog, poetname)
		poet_total_logs.append(finalstring)
		count = 0
		found = []
		totallog = 0
		#print "%s" %poet_total_logs
		#print "%s" %finalstring
	#poet_total_logs.sort(key=float)
	#print "%s" %poet_total_logs
	sorted_poet_totals = sorted(poet_total_logs)
	#print "%s" %sorted_poet_totals
	identified_name = sorted_poet_totals[0]
	identified_log = sorted_poet_totals[0]
	identified_name_arr = re.findall("[A-Z][a-z]+", identified_name)
	identified_name_final = " ".join(identified_name_arr)
	identified_log = re.search("-[0-9]+.[0-9]+", identified_log).group()
	identified_log = float(identified_log)
	
	#$identified_name =~ s/[^A-Z]+//;
        #chomp($identified_name);
        #($identified_log) = $identified_log =~ /(-[0-9]+.[0-9]+)/;

	#print "%s" %sorted_poet_totals
	print "%s most resembles the work of %s (log-probability=%.1f)" %(poemfile, identified_name_final, identified_log)
