#!/usr/bin/python -u

import os, re, glob, sys, math

wordgiven = sys.argv[1]

for file in glob.glob("poets/*.txt"):
        with open(file) as f:
                lines = f.readlines()
                found = []
                for line in lines:
                        line = re.sub('[^a-zA-Z ]', ' ', line)
                        line = re.sub(' +', ' ', line)
                        for word in line.split():
                                found.append(word)
                #print "%s" % found
                #print "%d" %len(found)
                count = 0
                for element in found:
                        eupper = element.upper()
                        elower = element.lower()
                        wordupper = wordgiven.upper()
                        wordlower = wordgiven.lower()
                        #print "comparing %s with %s and %s with %s" %(eupper, wordupper, elower, wordlower)
                        if (eupper == wordupper or elower == wordlower):
                                #print "match found"
                                count += 1
                poetname = re.sub(".*/", "", file)
                poetname = re.sub("\..*", "", poetname)
                poetname = re.sub("_", " ", poetname)
                decimal = (count / (len(found) * 1.0)) 
                #print "%4d/%6d = %.9f %s" %(count, len(found), decimal, poetname)
		lognum = ((count+1) / (len(found) * 1.0))
		log = math.log(lognum)
		print "log((%d+1)/%6d) = %8.4f %s" %(count, len(found), log, poetname)
