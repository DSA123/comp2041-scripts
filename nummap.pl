#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@text = split '\s+', $line;
	push @text, "\n";

	foreach $curr (@text) {
		if ($curr =~ /[0-9]+\.*[0-9]*/) {
			$before = $curr;
			$after = $curr;
			$num = $curr;
			$before =~ s/[0-9]+.*//;
			$after =~ s/^.*[0-9]+\.*[0-9]*//;
			$num =~ s/[^0-9\.]+//;
			if ($num =~ /\./) {
				$first = $num;
				$last = $num;
				$first =~ s/\..+//;
				$last =~ s/.+\.//;
				$last = "0.".$last;
				if ($last >= 0.50) {
					$first++;
				}
				$num = $first;
			}
			$curr = $before.$num.$after;
		}
		push @final, $curr; 
	
	}
}
push @lines, @final;

$seen = 0;
foreach $var (@lines) {
	if ($var =~ /\n/) {
		$seen = 1;
	}
	if ($seen == 0) {
		print $var." ";
	} else {
		print $var;
	}
	$seen = 0;
}
