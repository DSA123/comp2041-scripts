#!/usr/bin/perl -w

while ($line = <STDIN>) {
	chomp($line);
	@chars = split "", $line;
	push @allchars, @chars;
	push @allchars, "\n";
}

foreach $char (@allchars) {
	if ($char =~ /[aeiou]/) {
		$char = uc($char);
	} elsif ($char =~ /[AEIOU]/) {
		$char = lc($char);
	}
	print $char;
}
print "\n";
