#!/bin/sh

if test $# = 1
then
	code=$1
	letter=$1	
#letter=`echo "$1" | egrep -o "^."` 
else
	echo "Usage: $0 <prefix>" 1>&2
	exit 1
fi

ugrad=`wget -O- -q "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Undergraduate&descr=$letter" | egrep -o "[A-Z]{4}[0-9]{4}.*$" | egrep "$code" | sed 's/.html">/ /g' | cut -d"<" -f1 | sed 's/[A-Z]{4}[0-9]{4} +$//g' | egrep -o "[A-Z]{4}[0-9]{4} +.+$"`
pgrad=`wget -O- -q "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Postgraduate&descr=$letter" | egrep -o "[A-Z]{4}[0-9]{4}.*$" | egrep "$code" | sed 's/.html">/ /g' | cut -d"<" -f1 | sed 's/[A-Z]{4}[0-9]{4} +$//g' | egrep "[A-Z]{4}[0-9]{4} +.+"`
#all=`echo "$ugrad " ; echo "$pgrad"`
if [[ -z "$ugrad" ]] || [[ -z "$pgrad" ]]
then
	printf "%s%s" "$ugrad" "$pgrad" | sed -e 's/ *$//g' | sort | uniq
else
	printf "%s\n%s" "$ugrad" "$pgrad" | sed -e 's/ *$//g' | sort | uniq
fi
#printf "%s\n%s" "$ugrad" "$pgrad" | sed -e 's/ *$//g' | sort | uniq
#all=$ugrad$pgrad
#echo "$all" | sort | uniq
#echo "The code is $code"
#echo "The letter is $letter"
#echo "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Undergraduate&descr=$letter"
