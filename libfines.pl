#!/usr/bin/perl -w

$max = 0;
$swap = 0;
$count = 0;
while (<>) {
	if ($swap == 0) {
		print "Enter student name: ";
		$input = <STDIN>;
		chomp($input) if defined $input;
		$name = $input;
		$swap++;
		#push @names, $name;
	} else {
		print "Enter library fine: ";
		$input = <STDIN>;
		chomp($input) if defined $input;
		$fine = $input;
		$swap--;
		#$name = pop @names;
		$student{$name} += $fine;
	}
	$count++;
	if (($swap == 0) && ($count != 0)) {
		if ($student{$name} >= $max) {
			$max = $student{$name};
			$final = $name;
		}
	}
}
print "Expel ".$final." whose library fines total ".'$'.$student{$final}."\n";
