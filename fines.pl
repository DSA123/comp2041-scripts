#!/usr/bin/perl -w

$swap = 0;
$max = 0;
while ($input = <STDIN>) {
	chomp($input);
	if ($swap == 0) {
		print "Enter student name: ";
		$name = $input;
		push @names, $name;
		$swap++;
	} else {
		print "Enter library fine: ";
		$num = $input;
		$name = pop @names;
		$student{$name} += $num;
		if ($student{$name} >= $max) {
			$max = $student{$name};
			$maxstudent = $name;
		}
		$swap--;
	}
}

print "Expel ".$maxstudent.' whose library fines total $'.$student{$maxstudent};
