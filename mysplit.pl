#!/usr/bin/perl -w

sub my_split {
	($regex, $string) = @_;
	@list = ();
	while ($string =~ s/(.*?)$regex//) {
		push @list, $1;

	}
	push @list, $string if defined $string;
	return @list;
}

@a = my_split('\D+', "12er34ty56 ");
print join(",", @a)."\n";
