#!/bin/sh

#for loop to print Small files: and then every file name that wc -l < 10
#for loop to print Medium files: and then every file name that wc -l < 99
#for loop to print Large files: and then every remaining file.
#use tr "\n" " " to put all the output of wc on one line.
#set -x

count=0
found=0
for file in *
do
	i=`wc -l "$file" | tr "\n" " "`
	if [[ "$i" =~ ^[^0-9]*[0-9]{1}[^0-9]*$ ]]
	then
		if test "$count" -eq 0
		then 
			files=`echo $i  | tr -d "[0-9 ]"`
			printf "Small files: $files"
			#echo "Small files: \c"
			#echo -n "$i\c" | tr -d "[0-9 ]"
			#echo -n " \c"
			((count++))
			((found++))
		else
			files=`echo $i | tr -d "[0-9 ]"`
			printf " $files"
			#echo "$i\c" | tr -d "[0-9 ]"
			#echo " \c"
		fi
	fi 
done
if test "$found" -eq 0
then
	printf "Small files: "
	#echo "Small files: \c"
fi
echo " "
count=0
found=0
for file in *
do
        i=`wc -l "$file" | tr "\n" " "`
        if [[ "$i" =~ ^[^0-9]*[0-9]{2}[^0-9]*$ ]]
        then
                if test "$count" -eq 0
                then
                        files=`echo $i  | tr -d "[0-9 ]"`
                        printf "Medium-sized files: $files"
			#echo "Medium-sized files: \c"
                        #echo "$i\c" | tr -d "[0-9 ]"
                        #echo " \c"
                        ((count++))
			((found++))
                else
                        files=`echo $i | tr -d "[0-9 ]"`
                        printf " $files"
			#echo "$i\c" | tr -d "[0-9 ]"
                        #echo " \c"
                fi
        fi
done
if test "$found" -eq 0
then
	printf "Medium-sized files: "
	#echo "Medium-sized files: \c"
fi
echo " "
count=0
found=0
for file in *
do
        i=`wc -l "$file" | tr "\n" " "`
        if [[ "$i" =~ ^[^0-9]*[0-9]{3,}[^0-9]*$ ]]
        then
                if test "$count" -eq 0
                then
                        files=`echo $i  | tr -d "[0-9 ]"`
                        printf "Large files: $files"
			#echo "Large files: \c"
                        #echo "$i\c" | tr -d "[0-9 ]"
                        #echo " \c"
                        ((count++))
			((found++))
                else
                        files=`echo $i | tr -d "[0-9 ]"`
                        printf " $files"
			#echo "$i\c" | tr -d "[0-9 ]"
                        #echo " \c"
                fi
        fi
done
if test "$found" -eq 0
then
	printf "Large files: "
	#echo "Large files: \c"
fi
echo " "
