#!/bin/sh

#how to read LINE and count number of lines being piped in, while also using `uniq -c`. ` blah ` uses the most recent variable, so can use one or the other but not both?
#example test line:
#i=0;while test $i -lt 5; do echo $i; i=$((i + 1)); done|./shuffle.pl|./shuffle_test.sh
#wc for number of arguments and uniq to test every variable repeated only once
touch "tempfile.txt";
while IFS= read -r line; do
	printf '%s\n' "$line";
done > "tempfile.txt"
unique=`cat "tempfile.txt" | sort | uniq -c | cut -d" " -f4-`;
#unique=`uniq -c | egrep -o "[0-9]+.*"`;
#echo "$unique";
complete=false;
numlines=`cat "tempfile.txt" | wc -l | tr -d " "`;
#numlines=`wc -l | tr -d " "`;
echo "Number of lines input: $numlines";
echo "Searching for $numlines unique values...";
i=0;
while IFS= read -r line || [ -n "$line" ]; do
	if [[ "$line" =~ ^[1]{1}.*$ ]]
	then
		#echo "i value is: $i";
		#echo "$line";
		#echo "sucess!!";
		i=$((i + 1));
		#echo "i is $i\n";
	elif [[ "$line" =~ ^[0-9]+.*$ ]]
	then
		number=`echo "$line" | egrep -o "[0-9]+ +" | tr -d " "`;
		#echo "$number";
		i=`expr "$i" + "$number"`;
		#echo "i is $i\n";
	fi
	#echo "i is $i\n";
done <<<"$unique"
#echo "Number of lines: $numlines"
#echo "Number of i's: $i"
if test "$numlines" -eq "$i"; then
	echo "Success!! All $i values found";
else
	echo "Failed! Only found $i values";
fi
`rm tempfile.txt`
